module immgen(imm_out,imm_in,imm_sel);
parameter WIDTH_DATA_LENGTH = 32;

output	reg [WIDTH_DATA_LENGTH-1:0] imm_out;
input	[WIDTH_DATA_LENGTH-1:0] imm_in;
input	[2:0]imm_sel;
parameter [2:0]Iform = 3'b000, Sform = 3'b001, Bform = 3'b010, Uform = 3'b011, Jform = 3'b100; 

always @(imm_in or imm_sel)
begin
	case (imm_sel)
	  Iform: imm_out = {{20{imm_in[31]}},imm_in[31:20]};
	  Sform: imm_out = {{20{imm_in[31]}},imm_in[31:25],imm_in[11:7]};
	  Bform: imm_out = {{20{imm_in[31]}},imm_in[31],imm_in[7],imm_in[30:25],imm_in[11:8],1'b0};
	  Uform: imm_out = {imm_in[31:12],{12{1'b0}}};
	  Jform: if (imm_in[3]) imm_out = {{12{imm_in[31]}},imm_in[31],imm_in[19:12],imm_in[20],imm_in[30:21],1'b0};
	         else imm_out = {{20{imm_in[31]}},imm_in[31:20]};
	  default: imm_out = 0;
	endcase
end
endmodule