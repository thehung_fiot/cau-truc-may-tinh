module PC(input [31:0] PC_in, input Clk, 
output reg [31:0] PC_out);
 always @(posedge Clk)
 
 PC_out <= PC_in;
 
endmodule