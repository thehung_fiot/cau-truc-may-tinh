module cpu (clk ) ; 
  input clk;         
  wire clk;
 
  wire [31:0] pc_in, pc_out, pc_plus4_out, rs1_out, rs2_out, imm_out, alumux1_out, alumux2_out, aluout, dmem_out, wb_out ;
  wire [31:0] inst;
 
  wire [4:0]  rs1, rs2, rsd ;
  wire br_eq, br_lt, pcmux_sel, regfilemux_sel, cmpop, alumux1_sel, alumux2_sel, dmem_sel;
  
  wire [2:0] imm_sel;
  wire [1:0] wbmux_sel; 
  wire [3:0] aluop; 
  
  assign rs1 = inst[19:15];
  assign rs2 = inst[24:20];
  assign rsd = inst[11:7];
  assign imm_in = inst;
  
  
 ROMControl Romctrl(.inst(inst),.br_eq(br_eq),.br_lt(br_lt),.data(data),.pcmux_sel(pcmux_sel),.imm_sel(imm_sel),.br_un(br_un),.alumux1_sel(alumux1_sel),.alumux2_sel(alumux2_sel),.aluop(aluop),.dmem_sel(dmem_sel),.regfilemux_sel(regfilemux_sel),.wbmux_sel(wbmux_sel));
 
 PCmux pcMux(.out(pc_in),.in0(pc_plus4_out),.in1(aluout),.sel(pcmux_sel));
 
 PC pc( .PC_in(pc_in), .Clk(clk), .PC_out(pc_out));

PC4 pc4(.in(pc_out), .out(pc_plus4_out)) ;

IMEM imem(.inst(inst),.PC(pc_out));

regfile Regfile( .clock(clk), .rs1(rs1), .rs2(rs2), .rsd(rsd), .wb_out(wb_out),  .en_write(regfilemux_sel),  .rs1_out(rs1_out) , .rs2_out(rs2_out) );

immgen Immgen( .imm_in(inst), .imm_sel(imm_sel), .imm_out(imm_out));

branchcom BranchCom(.br_lt(br_lt),.br_eq(br_eq),.cmpop(br_un),.rs1_out(rs1_out),.rs2_out(rs2_out));

ALUmux1 A1(.out(alumux1_out),.in0(rs1_out),.in1(pc_out),.sel(alumux1_sel));

ALUmux2 A2(.out(alumux2_out),.in0(rs2_out),.in1(imm_out),.sel(alumux2_sel));

ALU alu( .ALUSel(aluop),  .aluout(aluout) ,	.alumux1_out(alumux1_out),	.alumux2_out(alumux2_out));

DMEM(.rs2_out(rs2_out), .dmem_out(dmem_out), .aluout(aluout), .dmem_sel(dmem_sel), .clk(clk)); 

Wbmux WBmux(.out(wb_out),.in0(dmem_out),.in1(aluout),.in2(pc_plus4_out),.sel(wbmux_sel));

endmodule 

