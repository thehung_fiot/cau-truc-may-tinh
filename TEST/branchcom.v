module branchcom(br_lt,br_eq,cmpop,rs1_out,rs2_out);
parameter WIDTH_DATA_LENGTH = 32;

output	reg br_lt;
output	reg br_eq;
input	wire [WIDTH_DATA_LENGTH-1:0] rs1_out,rs2_out;
input	wire cmpop;


//always @(cmpop or rs1_out or rs2_out)
always @(*)
begin
	if (~cmpop)
	  begin
	    if (rs1_out < rs2_out)
		begin
		  br_lt = 1;
		  br_eq = 0;
		end
	     else if(rs1_out == rs2_out) 
		begin
		  br_lt = 0;
		  br_eq = 1;
		end
		else 
		begin
		  br_lt = 0;
		  br_eq = 0;
		end
	  end
	else 
	  begin 
	    case ({rs1_out[31],rs2_out[31]})
        
		2'b00:  if (rs1_out < rs2_out)  begin br_lt = 1; br_eq = 0; end
			else if(rs1_out == rs2_out) begin br_lt = 0; br_eq = 1; end
			else  begin br_lt = 0; br_eq = 0; end	 
			 
		2'b10:  begin br_lt = 1; br_eq = 0; end
		2'b11:  if (rs1_out[30:0] < rs2_out[30:0]) begin br_lt = 1; br_eq = 0; end
			else if(rs1_out[30:0] == rs2_out[30:0]) begin br_lt = 0; br_eq = 1; end	
			else  begin br_lt = 0; br_eq = 0; end
		default:  begin br_lt = 0; br_eq = 0; end
        endcase
	  end
end
endmodule