module regfile(input clock, input [4:0] rs1,input [4:0] rs2,
input [4:0] rsd,input [31:0] wb_out,
 input en_write, output reg [31:0] rs1_out ,output reg [31:0] rs2_out );

// Register file storage
reg [31:0] registers[31:0];


// Read and write from register file
always @(posedge clock)
    if (en_write)
    begin
         
        registers[rsd] <= wb_out;
        rs1_out <= registers[rs1];
        rs2_out <= registers[rs2];
        registers[0]<=32'd0;
    end    
        
    else
    begin
          
        rs1_out <= registers[rs1];
        rs2_out <= registers[rs2];
        registers[0]<=32'd0;
    end    
        

// Output data if not writing. If we are writing,
// do not drive output pins. This is denoted
// by assigning 'z' to the output pins.

//reg [7:0] out_val;
//assign data = en_write ? 8'bz : out_val;

endmodule  