module	ROMControl(inst,br_eq,br_lt,data,pcmux_sel,imm_sel,br_un,alumux1_sel,alumux2_sel,aluop,dmem_sel,regfilemux_sel,wbmux_sel);

parameter	addrwidth=11;
parameter	datawidth=15;
parameter	instwidth=32;
input [instwidth-1:0]inst;
input 		br_eq;
input 		br_lt;

output 	reg	[datawidth-1:0]	data;
output 			pcmux_sel;	
output 	[2:0]	imm_sel;
output 			br_un;
output 			alumux1_sel;
output 			alumux2_sel;
output 	[3:0]	aluop;
output			dmem_sel;
output 			regfilemux_sel;
output	[1:0]	wbmux_sel;

wire [10:0]		addr;
assign addr={inst[30],inst[14:12],inst[6:2],br_eq,br_lt};

always@(addr)
begin
	casex(addr)	
//R-Format	
		11'b0_000_01100_x_x : data=15'b0_xxx_x_0_0_0000_0_1_01; // ADD
		11'b1_000_01100_x_x : data=15'b0_xxx_x_0_0_0001_0_1_01; // SUB
		11'b0_001_01100_x_x : data=15'b0_xxx_x_0_0_0010_0_1_01; // SLL
    	11'b0_010_01100_x_x : data=15'b0_xxx_x_0_0_0011_0_1_01; // SLT
		11'b0_011_01100_x_x : data=15'b0_xxx_x_0_0_0100_0_1_01; // SLTU
		11'b0_100_01100_x_x : data=15'b0_xxx_x_0_0_0101_0_1_01; // XOR
		11'b0_101_01100_x_x : data=15'b0_xxx_x_0_0_0110_0_1_01; // SRL
		11'b1_101_01100_x_x : data=15'b0_xxx_x_0_0_0111_0_1_01; // SRA
		11'b0_110_01100_x_x : data=15'b0_xxx_x_0_0_1000_0_1_01; // OR
		11'b0_111_01100_x_x : data=15'b0_xxx_x_0_0_1001_0_1_01; // AND
//I-Format
		11'b1_000_00100_0_0 : data=15'b0_000_0_0_1_0000_0_1_01; // ADDI
		11'bx_010_00100_x_x : data=15'b0_000_x_0_1_0010_0_1_01; // SLTI
		11'bx_011_00100_x_x : data=15'b0_000_x_0_1_0100_0_1_01; // SLTIU
		11'bx_100_00100_x_x : data=15'b0_000_x_0_1_0101_0_1_01; // XORI
		11'bx_110_00100_x_x : data=15'b0_000_x_0_1_1000_0_1_01; // ORI
		11'bx_111_00100_x_x : data=15'b0_000_x_0_1_1001_0_1_01; // ANDI
		11'b0_001_00100_x_x : data=15'b0_000_x_0_1_0010_0_1_01; // SLLI
		11'b0_101_00100_x_x : data=15'b0_000_x_0_1_0110_0_1_01; // SRLI
		11'b1_101_00100_x_x : data=15'b0_000_x_0_1_0111_0_1_01; // SRAI
//S_Format
		11'bx_000_01000_x_x : data=15'b0_001_x_0_1_0000_0_1_00; // LW
		11'bx_001_01000_x_x : data=15'b0_001_x_0_1_0000_1_0_xx; // SH ??
		11'bx_010_01000_x_x : data=15'b0_001_x_0_1_0000_1_0_xx; // SW
//B-Format
		
		11'bx_000_11000_0_x : data=15'b1_010_x_1_1_0000_0_0_xx; // BEQ
		11'bx_001_11000_0_x : data=15'b1_010_x_1_1_0000_0_0_xx; // BNE
		11'bx_100_11000_x_1 : data=15'b1_010_0_1_1_0000_0_0_xx; // BLT
		//11'bx_101_11000_x_					// BGE					
		11'bx_110_11000_x_1 : data=15'b1_010_1_1_1_0000_0_1_xx; // BLTU
		//11'bx_111_11000_    :					// BGEU
//U_Format  
		11'bx_xxx_00101_x_x : data=15'b0_011_x_1_1_0000_0_1_01; // AUIPC
		11'bx_xxx_01101_x_x : data=15'b0_011_x_0_1_0000_0_1_01; // LUI ??
//J_Format	
		11'bx_xxx_11011_x_x : data=15'b1_100_x_1_1_0000_0_1_10; // JAL
		11'bx_000_11001_x_x : data=15'b1_100_x_0_1_0000_0_1_10; // JALR
	endcase
end
	assign pcmux_sel	= data[14];
	assign imm_sel  	= data[13:11];
	assign br_un	 	= data[10];
	assign alumux1_sel	= data[9];
	assign alumux2_sel	= data[8];
	assign aluop		= data[7:4];
	assign dmem_sel		= data[3];
	assign regfilemux_sel	= data[2];
	assign wbmux_sel	= data[1:0];
endmodule


				
		

		