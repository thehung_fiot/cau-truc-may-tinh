module Wbmux(out,in0,in1,in2,sel);
parameter WIDTH_DATA_LENGTH = 32;
output reg	[WIDTH_DATA_LENGTH-1:0]  out;
input	[WIDTH_DATA_LENGTH-1:0] in0,in1,in2;
input	[1:0]sel;

always@(*)
begin


case (sel)
2'd0: out = in0;
2'd1: out = in1;
2'd2: out = in2;
endcase
 
end

endmodule