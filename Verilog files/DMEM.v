// DMem
//
// ADDRESS_WIDTH: the number of memory addresses is 2 to the power 
// of this number.
//
// DATA_WIDTH: the size of each element in the memory.
//
// Number of word bits DATA_WIDTH.
//
module DMEM(rs2_out,   // Input data into the memory
            dmem_out,  // Output data from the memory
            aluout,    // Address of data to be read/written
            dmem_sel,  // When high, causes write to take place on posedge
            clk);      // Clock

parameter ADDRESS_WIDTH = 32;
parameter DATA_WIDTH = 32;
parameter MEM_WIDTH_LENGTH = 32;
parameter MEM_DEPTH = 1024;
integer   CYCLE;
//-------------Input Ports-----------------------------
input [DATA_WIDTH-1:0]       rs2_out;
input [ADDRESS_WIDTH-1:0]    aluout;
input dmem_sel;
input clk;
//-------------Output Ports----------------------------
output reg  [DATA_WIDTH-1:0] dmem_out;
wire		[29:0] pWord;
wire		[1:0]  pByte;
//-------------Wires-----------------------------------
//-------------Other-----------------------------------
reg [MEM_WIDTH_LENGTH-1:0] mem_data [0:MEM_DEPTH-1];

//------------Code Starts Here-------------------------


assign		pWord = aluout[31:2];
assign		pByte = aluout[1:0];

always @(posedge clk)
begin
        #(CYCLE/10);
	if(~dmem_sel)
	   begin
	     if (pByte == 2'b00)
	       dmem_out <= mem_data[pWord];
	     else
	       dmem_out <= 'hz;		
	   end
	 else
	   begin      
	     mem_data[pWord] <= rs2_out;
           end
end
endmodule