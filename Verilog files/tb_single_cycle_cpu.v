module tb_single_cycle_cpu();
parameter [31:0]CYCLE = 200;  // CHU KY XUNG CLK
reg clk;
integer i;



cpu my_cpu(.clk(clk));

initial begin   // KHOI TAO BO NHO
  for (i = 0; i < (my_cpu.Regfile.MEM_DEPTH+1); i = i + 1)
    my_cpu.Regfile.registers[i] = 'h0;
  
  for (i = 0; i < (my_cpu.dmem.MEM_DEPTH+1); i = i + 1)
    my_cpu.dmem.mem_data[i] = 'h0;
	
  $readmemh("factorial.txt",my_cpu.imem.IMEM);      // FILE CHUA TAP LENH
  #(CYCLE/20);
end

initial begin	
  my_cpu.dmem.mem_data[2]  = 32'd4;      // NHAP SO CAN TINH GIAI THUA
  my_cpu.dmem.mem_data[3]  = 'd0;
  my_cpu.dmem.CYCLE        = CYCLE;
  my_cpu.Regfile.CYCLE     = CYCLE;

  #(CYCLE/20);
  $display("//////////////////////*********************************************//////////////////////");
  $display("//////////////////////***************BAT DAU MO PHONG**************//////////////////////");
  $display("INPUT:%0d",my_cpu.dmem.mem_data[2]) ;
  #(CYCLE/10);
end

initial begin        // GENERATE CLOCK
  clk = 0;
  forever #(CYCLE/2) clk = !clk; 
end

always@(posedge clk) begin
  #(CYCLE/20) $display("<%0t>TEMP = %0d",$time,my_cpu.Regfile.registers[5]) ;
  if ((my_cpu.dmem.mem_data[3]))
    begin	
	    #(3*CYCLE);
        $display("OUTPUT:%0d",my_cpu.dmem.mem_data[3]) ;
		$display("//////////////////////***************KET THUC MO PHONG**************//////////////////////");
		$display("//////////////////////*********************************************//////////////////////");
		$stop;
    end
  #(CYCLE/20);
end

endmodule