
module mux2to1(out,in0,in1,sel);
parameter WIDTH_DATA_LENGTH = 32;
output	 [WIDTH_DATA_LENGTH-1:0] out;
input	[WIDTH_DATA_LENGTH-1:0] in0,in1;
input	sel;

assign	out = (!sel)?in0:in1;

endmodule