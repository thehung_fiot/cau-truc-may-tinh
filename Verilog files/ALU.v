
module ALU(
	ALUSel,
	aluout ,
	alumux1_out,
	alumux2_out);
	input 			ALUSel,
					alumux1_out,
					alumux2_out;
	output 			aluout ;
	integer i;
	reg[31:0]		aluout ;
	reg [31:0] y;
	wire[31:0]		alumux1_out,
					alumux2_out;
					
	wire[3:0]		ALUSel;

       initial begin 	aluout =0;
         end

	// aluout  mux
	always @(ALUSel,alumux1_out,alumux2_out)
	begin
		case (ALUSel)	// synthesis full_case parallel_case
			0:	aluout  = alumux1_out + alumux2_out;			// ADD
			1:	aluout  = alumux1_out - alumux2_out;			// SUB
			2:	aluout  = alumux1_out << (alumux2_out); //SLL
			3:	// SLT
				begin 
				if (alumux1_out[31] != alumux2_out[31]) 
					begin
						if (alumux1_out[31] > alumux2_out[31]) 
							begin
								aluout = 1;
							end 
						else
							begin
								aluout = 0;
							end
					end 
					else 
						begin
							if (alumux1_out < alumux2_out)
								begin
								aluout = 1;
								end
							else
							begin
								aluout = 0;
							end
						end
				end
			4:	aluout  = alumux1_out < alumux2_out;  //SLTU
			5:	aluout  = alumux1_out ^ alumux2_out ;	// XOR
			6:	aluout  = alumux1_out >> (alumux2_out); //SRL
		/*	7:	begin // SRA
				y = alumux1_out;
				for (i = (alumux2_out); i > 0; i = i - 1) 
					begin
						y = {y[31],y[31:1]};
					end
						aluout   = y;
				end  */
		

			8:	    aluout  = alumux1_out | alumux2_out;		// OR
			9:  	aluout  = alumux1_out & alumux2_out;	// AND
		endcase
	end
endmodule
