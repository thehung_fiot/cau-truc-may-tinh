module single_cycle_cpu (clk) ; 
  input clk;       
 

  wire        clk;
  wire        pcmux_sel;
  wire [31:0] pc_in, pc_out, pc_plus4_out;
  wire [31:0] inst;
  wire [4:0]  rs1, rs2, rsd ;
  wire [31:0] rs1_out, rs2_out, imm_out; 
  wire        regfilemux_sel;
  wire [2:0]  imm_sel;
  wire        alumux1_sel, alumux2_sel;
  wire [31:0] alumux1_out, alumux2_out;
  wire [3:0]  aluop; 
  wire [31:0] aluout;
  wire        dmem_sel;
  wire [31:0] dmem_out;
  wire [1:0]  wbmux_sel; 
  wire [31:0] wb_out;
  wire        cmpop;
  wire        br_eq, br_lt;
  wire [14:0] data;
  wire [10:0] addr;
  

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   assign rs1 = inst[19:15];
   assign rs2 = inst[24:20];
   assign rsd = inst[11:7];
   assign addr = Romctrl.addr;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ROMControl Romctrl(.inst(inst),.br_eq(br_eq),.br_lt(br_lt),.data(data),.pcmux_sel(pcmux_sel),.imm_sel(imm_sel),.br_un(br_un),.alumux1_sel(alumux1_sel),.alumux2_sel(alumux2_sel),.aluop(aluop),.dmem_sel(dmem_sel),.regfilemux_sel(regfilemux_sel),.wbmux_sel(wbmux_sel));
 
mux2to1 pcMux(.out(pc_in),.in0(pc_plus4_out),.in1(aluout),.sel(pcmux_sel));
 
PC pc1(.PC_in(pc_in),.Clk(clk),.PC_out(pc_out));

PC4 pc4(.in(pc_out), .out(pc_plus4_out)) ;

IMEM imem(.inst(inst),  .PC(pc_out));

regfile Regfile( .clock(clk), .rs1(rs1), .rs2(rs2), .rsd(rsd), .wb_out(wb_out),  .en_write(regfilemux_sel),  .rs1_out(rs1_out) , .rs2_out(rs2_out) );

immgen Immgen( .imm_in(inst), .imm_sel(imm_sel), .imm_out(imm_out));

branchcom BranchCom(.br_lt(br_lt),.br_eq(br_eq),.cmpop(br_un),.rs1_out(rs1_out),.rs2_out(rs2_out));

mux2to1 A1(.out(alumux1_out),.in0(rs1_out),.in1(pc_out),.sel(alumux1_sel));

mux2to1 A2(.out(alumux2_out),.in0(rs2_out),.in1(imm_out),.sel(alumux2_sel));

ALU alu( .ALUSel(aluop),  .aluout(aluout) ,	.alumux1_out(alumux1_out),	.alumux2_out(alumux2_out));

DMEM dmem(.rs2_out(rs2_out), .dmem_out(dmem_out), .aluout(aluout), .dmem_sel(dmem_sel), .clk(clk)); 

Wbmux WBmux(.out(wb_out),.in0(dmem_out),.in1(aluout),.in2(pc_plus4_out),.sel(wbmux_sel));





endmodule 

